# Stack and heap buffer overrun examples, and how to detect them
Note that valgrind should detect all the given buffer overrun examples.

## Prerequisites
The C++ IDE [juCi++](https://gitlab.com/cppit/jucipp) should be installed.

## Installing dependencies

### Debian based distributions
`sudo apt-get install valgrind`

### Arch Linux based distributions
`sudo pacman -S valgrind`

### OS X
`brew install valgrind`

## Download and open
```sh
git clone https://gitlab.com/ntnu-tdat3020/buffer-overflow
cd buffer-overflow
juci . example*.cpp
```
