#include <iostream>

using namespace std;

/** Example showing stack buffer overrun that is detected through
 * a static analyzer.
 *
 * On MacOS (replace <version> with llvm version):
 *   ln -s /opt/homebrew/opt/llvm/bin/scan-build /usr/local/bin
 *   ln -s /opt/homebrew/opt/llvm/bin/scan-view /usr/local/bin
 *
 * Steps to reproduce warning:
 *   cd build && rm -r * && scan-build cmake .. && scan-build make
 **/
int main() {
  char str[5] = {'t', 'e', 's', 't', '\0'};

  cout << str[7] << endl;
}
