#include <iostream>
#include <string>
#include <vector>

using namespace std;

/**
 * Example showing heap buffer overrun that might be detected through valgrind
 *
 * Prerequisite: Linux, valgrind
 *
 * To reproduce:
 *   g++ -O0 -g example5.cpp -o example5 && valgrind ./example5
 **/
int main() {
  string str = "test";

  cout << str[7] << endl;
}
