#include <iostream>
#include <string>
#include <vector>

using namespace std;

/**
 * Example showing heap buffer overrun that is detected through
 * AddressSanitizer (CMakeLists.txt contains: -fsanitize=address).
 **/
int main() {
  vector<char> str = {'t', 'e', 's', 't', '\0'};

  cout << str[7] << endl;
}
